export namespace NumbertAPI {
  function newHttpAgent(): any | XMLHttpRequest {
    if ("XDomainRequest" in global && !/MSIE 1/.test(navigator.userAgent))
      //@ts-ignore
      return new global.XDomainRequest();
    if ("XDomainRequest" in XMLHttpRequest) return new global.XMLHttpRequest();
  }
  class CancelablePromise<T> extends Promise<T> {
    cancel: () => void;
    constructor(
      executor: (
        resolve: (value?: T | PromiseLike<T> | undefined) => void,
        reject: (reason?: any) => void
      ) => void,
      cancel: (reason?: any) => void
    ) {
      super(executor);
      this.cancel = cancel;
    }
  }
  export function call(
    timeout: number,
    base: string,
    method: string,
    data: object,
    token?: string
  ): CancelablePromise<object | null> {
    var agent = newHttpAgent();
    agent.open("POST", base + method, true);
    return new CancelablePromise((resolve, reject) => {
      agent.timeout = timeout;
      agent.onerror = () => {
        reject(new Error("API call: Error"));
      };
      agent.ontimeout = () => {
        reject(new Error("API call: Timeout"));
      };
      agent.onabort = () => {
        reject(new Error("API call: Abort"));
      };
      agent.onreadystatechange = () => {
        try {
          if (agent.readyState != 4) return;
          switch (agent.status) {
            case undefined:
            case null:
            case "0":
            case 0:
              throw new Error("API call: Error");
          }
          if (agent.status == 202) {
            resolve(null);
          }
          var result = JSON.parse(agent.responseText);
          if ("error" in result) throw result.error;
          if (agent.status != 200)
            throw new Error(agent.statusText || agent.status.toString());
          resolve(result);
        } catch (e) {
          if (typeof e === "string") e = new Error(e);
          reject(e);
        }
      };
      if (token) agent.setRequestHeader("Authorization", "Bearer " + token);
      agent.setRequestHeader("Content-Type", "application/json");
      agent.send(JSON.stringify(data));
    }, agent.abort);
  }
}
